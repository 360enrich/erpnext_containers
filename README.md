### Add Apps to Containers

1. Add app(s) in `ci/clone-apps.sh`, e.g. `git clone --depth 1 --branch master https://github.com/frappe/twilio-integration repos/twilio_integration`
2. Make changes in `containers/nginx/Dockerfile`
    1. Install app with `/install_app twilio_integration  && \` before `prepare_production` is executed
    2. Add `twilio_integration` to `apps.txt`, `RUN echo -n "...\ntwilio_integration" >> /var/www/html/apps.txt`
3. Make changes in `containers/worker/Dockerfile`
    1. Install app with command: `/home/frappe/frappe-bench/env/bin/pip install -e /home/frappe/frappe-bench/apps/twilio_integration`
